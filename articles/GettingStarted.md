# Using max.ds

``max.ds`` is a MaxLib Helper Library that offers additional data structures.

* [Installation](Installation.md)
* [Data Structures](DataStructures.md)

## Max Dependencies:

``max.ds`` does not depend on any other Max libraries.