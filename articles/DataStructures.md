# Using Data Structures

## Interfaces

This is a list of how to use the interfaces that may be required to use some of these Data Structures.

### IComparable

Some data structures need to compare the value between two objects. Here is an example on how to implement ``IComparable``:

```
public class TestObject : IComparable<TestObject>
{
	public int CompareVal { get; set; }

	//...

	public int CompareTo(TestObject other)
	{
		if(CompareVal > other.CompareVal){
			// larger
			return 1;
		} else if (CompareVal < other.CompareVal){
			// smaller
			return -1;
		}

		// they are equal in value;
		return 0;
	}
}
```

## Data Structures

### Priority Queue

Priority Queues are data structures that work like queues, but their contents are always organized based on the ``CompareTo`` function of its contents.

To use a priority queue, your object must implement the ``IComparable`` interface. 

When you enqueue the Priority Queue, the object will be organized from smallest to largest. 

```
// ...

// instantiate PQ
PriorityQueue<TestObject> PQ = new PriorityQueue<TestObject>();

// set compare values
obj1.CompareVal = 5;
obj2.CompareVal = 9;
obj3.CompareVal = 1;

//enqueue the objects
PQ.Enqueue(obj1);
PQ.Enqueue(obj2);
PQ.Enqueue(obj3);
```

When you dequeue the Priority Queue, the last (largest) object in the queue will be returned.

```
while(PQ.Count > 0){
	// prints " Value 9 Value 5 Value 1"
	Console.Write(" Value: " + PQ.Dequeue.CompareVal());
}
```