# max.ds

``max.ds`` is a MaxLib Helper Library that offers additional data structures.

* [Documentation](https://labadore64.gitlab.io/max.ds/)
* [Source](https://gitlab.com/labadore64/max.ds/)