﻿using System;
using System.Collections.Generic;

namespace max.ds
{
    /// <summary>
    /// Represents an organized set of values.
    /// </summary>
    /// <typeparam name="T">The type of values in the Priority Queue.</typeparam>
    public class PriorityQueue<T> where T : IComparable<T>
    {
        private List<T> data;

        /// <summary>
        /// Instantiates an empty priority queue.
        /// </summary>
        public PriorityQueue()
        {
            this.data = new List<T>();
        }

        /// <summary>
        /// Instantiates a priority queue with the provided collection of items.
        /// </summary>
        /// <param name="Collection">Items</param>
        public PriorityQueue(ICollection<T> Collection)
        {
            foreach(T t in Collection)
            {
                Enqueue(t);
            }
        }

        /// <summary>
        /// Enqueues the item into the queue. The location in the queue is based on its CompareTo value.
        /// </summary>
        /// <param name="item">Item</param>
        public void Enqueue(T item)
        {
            data.Add(item);
            int ci = data.Count - 1; // child index; start at end
            while (ci > 0)
            {
                int pi = (ci - 1) / 2; // parent index
                if (data[ci].CompareTo(data[pi]) >= 0) break; // child item is larger than (or equal) parent so we're done
                T tmp = data[ci]; data[ci] = data[pi]; data[pi] = tmp;
                ci = pi;
            }
        }

        /// <summary>
        /// Dequeues the last value in the PriorityQueue.
        /// </summary>
        /// <returns>The dequeued value.</returns>
        public T Dequeue()
        {
            // assumes pq is not empty; up to calling code
            int li = data.Count - 1; // last index (before removal)
            T frontItem = data[0];   // fetch the front
            data[0] = data[li];
            data.RemoveAt(li);

            --li; // last index (after removal)
            int pi = 0; // parent index. start at front of pq
            while (true)
            {
                int ci = pi * 2 + 1; // left child index of parent
                if (ci > li) break;  // no children so done
                int rc = ci + 1;     // right child
                if (rc <= li && data[rc].CompareTo(data[ci]) < 0) // if there is a rc (ci + 1), and it is smaller than left child, use the rc instead
                    ci = rc;
                if (data[pi].CompareTo(data[ci]) <= 0) break; // parent is smaller than (or equal to) smallest child so done
                T tmp = data[pi]; data[pi] = data[ci]; data[ci] = tmp; // swap parent and child
                pi = ci;
            }
            return frontItem;
        }

        /// <summary>
        /// Returns the last value in the PriorityQueue without dequeueing it.
        /// </summary>
        /// <returns>The last value.</returns>
        public T Peek()
        {
            T frontItem = data[0];
            return frontItem;
        }

        /// <summary>
        /// The number of items in the queue.
        /// </summary>
        /// <returns>The dequeued value.</returns>
        public int Count()
        {
            return data.Count;
        }

        /// <summary>
        /// Returns the data in the PriorityQueue as a string.
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < data.Count; ++i)
                s += data[i].ToString() + " ";
            s += "count = " + data.Count;
            return s;
        }

        /// <summary>
        /// Returns if the queue satisfies the heap property.
        /// </summary>
        /// <returns>True/False</returns>
        public bool IsConsistent()
        {
            // is the heap property true for all data?
            if (data.Count == 0) return true;
            int li = data.Count - 1; // last index
            for (int pi = 0; pi < data.Count; ++pi) // each parent index
            {
                int lci = 2 * pi + 1; // left child index
                int rci = 2 * pi + 2; // right child index

                if (lci <= li && data[pi].CompareTo(data[lci]) > 0) return false; // if lc exists and it's greater than parent then bad.
                if (rci <= li && data[pi].CompareTo(data[rci]) > 0) return false; // check the right child too.
            }
            return true; // passed all checks
        } 
    } 

}
